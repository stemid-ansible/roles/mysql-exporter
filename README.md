# Mysql prometheus exporter Ansible role

* This role does not create the user and grant required, because it's meant to be used in a playbook that might have a different way of connecting to the mysql server. So better to let the playbooks set that up in my case.
* This role is a traditional install under /var/opt, no containers.
* It's meant to run one instance per DB node, so connect from localhost.
* Set the following;

* ``mysql_exporter_username`` - Used both as the Unix system user for the mysql-exporter process, and the MySQL user.
* ``mysql_exporter_password`` - The password for the user.
* ``mysql_exporter_install`` (Default: True) - Disable download archive and install mysql-exporter step.
* ``mysql_exporter_setup`` (Default: True) - Disable my.cnf config install step.
